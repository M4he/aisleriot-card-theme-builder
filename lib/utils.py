from PIL import Image
import struct
import math
import os


# replace the color value of all pixels for the given 'image'
# by the specified (r, g, b) color while preserving the alpha
def replace_color(image, color):
    r, g, b = hex_color_to_rgb(color)
    pixels = image.load()
    width, height = image.size
    for x in xrange(width):
        for y in xrange(height):
            p = pixels[x, y]
            _, _, _, a = p
            pixels[x, y] = (r, g, b, a)
    return


# invert the alpha channel of an image
def invert_alpha(image):
    pixels = image.load()
    width, height = image.size
    for x in xrange(width):
        for y in xrange(height):
            p = pixels[x, y]
            r, g, b, a = p
            a = 255 - a
            pixels[x, y] = (r, g, b, a)
    return


# trim transparent pixels from an Image instance on all sides
# returns an Image instance
def trim_image(image):
    pixels = image.load()
    width, height = image.size
    max_x = max_y = 0
    min_y = height
    min_x = width

    # find the corners that bound the letter by looking for
    # non-transparent pixels
    for x in xrange(width):
        for y in xrange(height):
            p = pixels[x, y]
            _, _, _, a = p
            if a != 0:
                min_x = min(x, min_x)
                min_y = min(y, min_y)
                max_x = max(x, max_x)
                max_y = max(y, max_y)
    return image.crop((min_x, min_y, max_x, max_y))


def load_image_from_file(image_path):
    try:
        return Image.open(image_path)
    except:
        return None


# returns an image of the specified 'width' and 'height' where the
# given 'image' has been fit into using a zoomed-fill method
def fit_image_into_dimension(image, width, height):
    if image.width == width and image.height == height:
        return image

    w, h = image.size

    # scale image to fit
    new_ratio = max(float(width)/float(w), float(height)/float(h))
    new_size = (int(math.ceil(w*new_ratio)), int(math.ceil(h*new_ratio)))

    image = image.resize(new_size, Image.ANTIALIAS)
    w, h = image.size
    draw_offset = ((width - w) / 2, (height - h) / 2)
    buf = Image.new('RGBA', (width, height), (0, 0, 0, 0))
    buf.paste(image, draw_offset)
    return buf


# converts a color in hex notation (e.g. '#FFAABB') to an RGB tuple
def hex_color_to_rgb(hex_str):
    if hex_str.startswith('#'):
        # cut the hash sign
        hex_str = hex_str[1:]
    return struct.unpack('BBB', hex_str.decode('hex'))


# converts a color in hex notation (e.g. '#FFAABBCC') to an RGBA tuple
def hex_color_to_rgba(hex_str):
    if hex_str.startswith('#'):
        # cut the hash sign
        hex_str = hex_str[1:]
    return struct.unpack('BBBB', hex_str.decode('hex'))


# REPLACMENT for Image.paste()
# draws 'foreground' on 'background' at 'pos' and returns the resulting
# Image - used instead of Image.paste()
#
# The problem with Image.paste() is incorrect alpha handling
# this will result in semi-transparent areas where should be none
# if layering multiple RGBA layers on top of each other
# Image.alpha_composite() fixes this but doesn't allow for positioning
# values so we have to work around this
def image_paste_with_alpha(background, foreground, pos):
    temp = Image.new('RGBA', background.size, (0, 0, 0, 0))
    temp.paste(foreground, pos)
    return Image.alpha_composite(background, temp)


# ensure a directory exists, create it if necessary
def ensure_directory(path):
    try:
        os.makedirs(path)
    except:
        pass
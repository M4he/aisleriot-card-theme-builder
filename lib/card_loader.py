import os
import utils
import random
from ar_const import ranks, suites


# abstract parent class, use one of the child classes instead!
class CardImageLoader(object):

    def __init__(self, style_dir):
        self.style_dir = style_dir
        self.img_dir = os.path.join(style_dir, 'artwork')
        self._try_mask()
        self._try_frame()

    # load the card mask if exists
    def _try_mask(self):
        self.mask_path = os.path.join(self.style_dir, 'mask.png')
        self.has_mask = False
        if os.path.exists(self.mask_path):
            self.mask = utils.load_image_from_file(self.mask_path)
            utils.invert_alpha(self.mask)
            self.has_mask = True

    # load the card frame if exists
    def _try_frame(self):
        self.frame_path = os.path.join(self.style_dir, 'frame.png')
        self.has_frame = False
        if os.path.exists(self.frame_path):
            self.frame = utils.load_image_from_file(self.frame_path)
            self.has_frame = True

    def get_card_image(self, suite, rank):
        raise NotImplementedError()

    def get_asset_image(self, name):
        path = os.path.join(self.style_dir, name)
        return utils.load_image_from_file(path)

# card image loader that assigns card images randomly
class RandomCardImageLoader(CardImageLoader):

    def __init__(self, style_dir):
        super(RandomCardImageLoader, self).__init__(style_dir)
        self.img_list = os.listdir(self.img_dir)
        random.shuffle(self.img_list)

    def get_card_image(self, suite, rank):
        return utils.load_image_from_file(
            os.path.join(self.img_dir, self.img_list.pop())
        )

    def get_asset_image(self, name):
        if 'joker' in name:
            return utils.load_image_from_file(
                os.path.join(self.img_dir, self.img_list.pop())
            )
        else:
            return super(RandomCardImageLoader, self).get_asset_image(name)


# card image loader that uses the '<suite>-<rank>' notation to identify images
class FixedCardImageLoader(CardImageLoader):

    def __init__(self, style_dir):
        super(FixedCardImageLoader, self).__init__(style_dir)
        self.img_list = os.listdir(self.img_dir)
        for suite in suites:
            for rank in ranks:
                card_name = "%s-%s.png" % (suite, rank)
                if card_name not in self.img_list:
                    card_name = "%s-%s.jpg" % (suite, rank)
                    if card_name not in self.img_list:
                        raise Exception(
                            "ERROR: card artwork '%s-%s' not found "
                            "(mandatory for 'fixed' enum-mode)!" %
                            (suite, rank)
                        )

    def get_card_image(self, suite, rank):
        card_name = "%s-%s.png" % (suite, rank)
        if card_name not in self.img_list:
            card_name = "%s-%s.jpg" % (suite, rank)
        return utils.load_image_from_file(
            os.path.join(self.img_dir, card_name)
        )

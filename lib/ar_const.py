# -*- coding: utf-8 -*-

# definitions of suites, ranks and extra cards, terminology used
# as per Aisleriot's internal definitions
ranks = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '10',
    'jack',
    'queen',
    'king'
]
suites = [
    'club',
    'diamond',
    'heart',
    'spade'
]
extras = [
    'joker_black',
    'joker_red',
    'back',
    'slot'
]

icons = {
    'club': u'♣',
    'diamond': u'♦',
    'heart': u'♥',
    'spade': u'♠'
}
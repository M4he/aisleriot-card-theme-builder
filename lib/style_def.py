import os


class StyleDef(object):

    def __init__(self, path):
        self.values = {
            'width': None,  # int
            'height': None,  # int
            'font': None,  # str (font, size)
            'icon-font': None,  # str (font, size)
            'enum-mode': None,  # str [random | fixed]
            'render-sizes': None,  # int list
            'heart-color': None,  # str
            'diamond-color': None,  # str
            'spade-color': None,  # str
            'club-color': None,  # str
            'shadow-color': None,  # str
            'shadow-radius': None,  # int
            'shadow-intensity': None,  # int
            'x-inset': None,  # int
            'y-inset': None,  # int
        }
        self.path = path
        self.sizes = []
        self.parse(path)

        # style name equals the name of the parent directory of the style.def
        parent_dir = os.path.abspath(os.path.join(path, os.pardir))
        self.name = os.path.basename(parent_dir)

    # return a corresponding height value for a given 'width' value
    # while preserving the aspect ratio of the original width and height
    def calculate_height_for_width(self, width):
        # new_height = (orig_height / orig_width) * new_width
        return int(round(
            float(
                self.values['height'])/float(self.values['width'])*float(width
            )
        ))

    def parse(self, path):
        with open(path, 'r') as f:
            for line in f.readlines():
                if line.startswith('#'):
                    # skip comment lines
                    continue
                key, value = line.split(':', 1)
                if key in self.values:
                    # catch integers
                    if key in [
                        'width', 'height', 'x-inset', 'y-inset',
                        'shadow-radius', 'shadow-intensity'
                    ]:
                        self.values[key] = int(value.rstrip())
                    else:
                        self.values[key] = value.rstrip()
                else:
                    print(
                        "WARNING: style definition '%s' unrecognized" %
                        key
                    )
        for k in self.values:
            if self.values[k] is None:
                raise Exception(
                    "Style error: no value for '%s' provided!" %
                    k
                )
        for rsize in self.values['render-sizes'].split(','):
            rsize = rsize.lstrip().rstrip()  # trim whitespaces
            w = int(rsize)
            if w == self.values['width']:
                print(
                    "WARNING: skipping duplicate additional size '%s'" %
                    str(w)
                )
                continue
            h = self.calculate_height_for_width(w)
            self.sizes.append((w, h))
        # finally, add original size
        self.sizes.append((self.values['width'], self.values['height']))

        # turn values into class attributes
        for k in self.values:
            setattr(self, k.replace('-', '_'), self.values[k])

    # generates a theme metadata file formatted in the .card-theme format
    # to be parsed as a theme file for the prerendered theme by Aisleriot
    def generate_theme_file(self, file_path):
        content = ''
        content += "[Card Theme]\n"
        content += "Sizes="
        for size in self.sizes:
            content += "%s;" % str(size[0])  # width only
        for size in self.sizes:
            content += "\n\n[%s]\n" % str(size[0])
            content += "Width=%s\n" % str(size[0])
            content += "Height=%s" % str(size[1])
        with open(file_path, 'w') as f:
            f.writelines(content)

from PIL import Image, ImageDraw, ImageFont, ImageFilter
from ar_const import ranks, suites, extras, icons
import utils
import os


class CardImage(object):

    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.image = Image.new('RGBA', (width, height), (0, 0, 0, 0))

    # draws the given 'image' onto the card
    def apply_image(self, image, x, y):
        self.image = utils.image_paste_with_alpha(self.image, image, (x, y))

    # applies an image provided as 'mask' as the alpha layer
    # (this is used to cut the rounded corners of the cards)
    def apply_mask(self, mask):
        background = Image.new('RGBA', (self.width, self.height), (0, 0, 0, 0))
        self.image = Image.composite(background, self.image, mask)


class CardRenderer(object):

    def __init__(self, loader, style, out_dir):
        self.loader = loader
        self.style = style
        self.out_dir = out_dir

        self.label_font, self.label_size = self.style.font.split(',')
        self.label_size = int(self.label_size.lstrip().rstrip())
        self.icon_font, self.icon_size = self.style.icon_font.split(',')
        self.icon_size = int(self.icon_size.lstrip().rstrip())
        self.width, self.height = self.style.width, self.style.height
        self.x_inset, self.y_inset = style.x_inset, style.y_inset

        self.card = None

        self.render_cache = {}

    # draw a text into an image buffer using the specified 'color'
    # returns an Image instance
    def _draw_text_as_image(self, text, color, is_icon=False):

        ident = "%s:%s" % (text, color)
        if ident in self.render_cache:
            return self.render_cache[ident]

        shadow_radius = self.style.shadow_radius
        shadow_intensity_steps = self.style.shadow_intensity
        shadow_color = utils.hex_color_to_rgba(self.style.shadow_color)

        # path of the style directory
        sp = os.path.abspath(os.path.join(self.style.path, os.pardir))

        if is_icon:
            font_path = os.path.join(sp, self.icon_font) 
            font_size = self.icon_size
        else:
            font_path = os.path.join(sp, self.label_font) 
            font_size = self.label_size
        font = ImageFont.truetype(font_path, font_size)
        text_width, text_height = font.getsize(text)

        # oversized buffer, because the shadow will bleed and extend the image!
        # (will be trimmed later)
        canvas_size = (
            text_width + 10*shadow_radius,
            text_height + 10*shadow_radius
        )
        # center any drawn text within the oversized buffer
        draw_offset = (5*shadow_radius, 5*shadow_radius)

        def draw_shadow(image):
            draw = ImageDraw.Draw(image)
            draw.text(
                draw_offset,
                text,
                font=font,
                fill=shadow_color
            )
            buf = image.filter(ImageFilter.GaussianBlur(radius=shadow_radius))
            # we paste the blurred shadow ontop of itself a few times to intensify
            for k in xrange(shadow_intensity_steps):
                buf = utils.image_paste_with_alpha(buf, buf, (0, 0))
            return buf

        # initialize the image with full transparency BUT use the shadow color as bg
        # (because on the blurred areas the shadow's edges will bleed into the
        # background color, which we set to the shadow color here for that reason)
        r, g, b, _ = shadow_color
        image = Image.new('RGBA', canvas_size, (r, g, b, 0))
        
        # shadow
        image = draw_shadow(image)

        # actual text
        draw = ImageDraw.Draw(image)
        draw.text(draw_offset, text, font=font, fill=color)

        # finally, trim the oversized image
        image = utils.trim_image(image)
        self.render_cache[ident] = image
        return image

    # draws a glyph (suite or rank indicator) on a card's corner
    # 'pos' may be one of {tl, tr, bl, br}
    # 'text_style' is a tuple of (font_path, font_size, font_color)
    # 'coords' is a tuple of (card_width, card_height, x_inset, y_inset)
    def _draw_glyph_on_card(self, glyph, pos, color, is_icon=False):
        width, height = self.card.image.size
        x_inset = self.style.x_inset
        y_inset = self.style.y_inset

        # temporary offscreen drawing buffer
        buf = self._draw_text_as_image(
            glyph,
            color,
            is_icon
        )
        # buf = add_shadow_to_image(buf, 5, '#FFFFFF')
        w, h = buf.size

        # corner positioning
        # x_inset and y_inset are the distance of the glyph's center to the edge
        if pos == 'tl':
            draw_x, draw_y = x_inset - (w/2), y_inset - (h/2)
        elif pos == 'tr':
            draw_x, draw_y = width - x_inset - (w/2), y_inset - (h/2)
        elif pos == 'bl':
            draw_x, draw_y = x_inset - (w/2), height - y_inset - (h/2)
        elif pos == 'br':
            draw_x, draw_y = width - x_inset - (w/2), height - y_inset - (h/2)
        else:
            raise Exception("Position type '%s' unrecognized" % str(pos))

        # draw onto card
        self.card.apply_image(buf, draw_x, draw_y)

    # renders the given 'card' in all sizes as specified by the loaded style
    # definition using the filename as specified via 'name'
    def _render_to_all_sizes(self, name):
        for render_size in self.style.sizes:
            render_w, render_h = render_size

            # only resize if size differs from original
            if render_w != self.width:
                result_img = self.card.image.resize(
                    render_size, Image.ANTIALIAS
                )
            else:
                result_img = self.card.image

            size_output_dir = os.path.join(
                self.out_dir, self.style.name, str(render_w)
            )
            utils.ensure_directory(size_output_dir)

            # save as final card texture
            result_img_path = os.path.join(size_output_dir, name)
            result_img.save(result_img_path)

    def render_card(self, suite, rank):
        suite_color = utils.hex_color_to_rgb(getattr(self.style, suite + '_color'))

        self.card = CardImage(self.width, self.height)

        # draw main artwork
        img = self.loader.get_card_image(suite, rank)
        img = utils.fit_image_into_dimension(img, self.width, self.height)
        self.card.apply_image(img, 0, 0)

        # style definitions and values for drawing
        rank_glyph = (
            rank
            .replace('jack', 'J')
            .replace('queen', 'Q')
            .replace('king', 'K')
        )
        # substitute '1' with 'A'
        rank_glyph = (rank_glyph == '1') and 'A' or rank_glyph

        if self.loader.has_frame:
            self.card.apply_image(self.loader.frame, 0, 0)

        # top left, rank
        self._draw_glyph_on_card(
            rank_glyph,
            'tl',
            suite_color,
            is_icon=False
        )
        # top right, suite
        self._draw_glyph_on_card(
            icons[suite],
            'tr',
            suite_color,
            is_icon=True
        )
        # bottom left, suite
        self._draw_glyph_on_card(
            icons[suite],
            'bl',
            suite_color,
            is_icon=True
        )
        # bottom right, rank
        self._draw_glyph_on_card(
            rank_glyph,
            'br',
            suite_color,
            is_icon=False
        )

        if self.loader.has_mask:
            self.card.apply_mask(self.loader.mask)

        self._render_to_all_sizes("%s-%s.png" % (suite, rank))

    def render_extras(self):
        for extra in extras:
            self.card = CardImage(self.width, self.height)

            filename = "%s.png" % extra
            img = self.loader.get_asset_image(filename)
            if img is None:
                filename = "%s.jpg" % extra
                img = self.loader.get_asset_image(filename)
            img = utils.fit_image_into_dimension(img, self.width, self.height)
            self.card.apply_image(img, 0, 0)

            if self.loader.has_frame and extra not in ['slot', 'back']:
                self.card.apply_image(self.loader.frame, 0, 0)

            if self.loader.has_mask:
                self.card.apply_mask(self.loader.mask)

            self._render_to_all_sizes(filename.replace('.jpg', '.png'))
#!/bin/python
# -*- coding: utf-8 -*-
from lib.style_def import StyleDef
from lib.card_renderer import CardRenderer
from lib.card_loader import FixedCardImageLoader, RandomCardImageLoader
from lib.ar_const import ranks, suites
import argparse
import utils
import os

ap = argparse.ArgumentParser()
ap.add_argument(
    "-i",
    dest="input",
    type=str,
    metavar="INPUT",
    help=(
        "Input directory, where the 'style.def' file and 'artwork' folder "
        "are located as well as any other extra source asset."
    ),
    required=True
)

ap.add_argument(
    "-o",
    dest="output",
    type=str,
    metavar="OUTPUT",
    help=(
        "Output directory, where .card-theme file and corresponding "
        "folder will be stored."
    ),
    required=True
)
args = ap.parse_args()

if not os.path.exists(args.input) or not os.path.isdir(args.input):
    raise Exception(
        "'%s' is not a valid input directory!" % str(args.input)
    )

style = StyleDef(os.path.join(args.input, 'style.def'))
print("INFO: output directory: '%s'" % os.path.abspath(args.output))
utils.ensure_directory(args.output)

if style.enum_mode == 'fixed':
    img_loader = FixedCardImageLoader(args.input)
else:
    img_loader = RandomCardImageLoader(args.input)

renderer = CardRenderer(img_loader, style, args.output)
renderer.render_extras()
for suite in suites:
    for rank in ranks:
        renderer.render_card(suite, rank)

# finally, write the .card-theme file for Aisleriot
theme_file_path = os.path.join(args.output, style.name + '.card-theme')
style.generate_theme_file(theme_file_path)

